/* Adiciona a imagem do usuario */
$(()=>{
  var pic = $('#homepage-link i').css('background-image');
  if (pic) {
    pic = pic.replace(/_icon\.([a-z0-9]{3,4}['"]?\)?)$/, '_big.$1');
    $('<div id="user-toggle"></div>')
      .css('background-image', pic)
      .prependTo('#user')
      .click(function(){ $('#user').toggleClass('open')});
  }
});

/* registra scroll da pagina  */
(function() {
  var $window = $(window);
  var $html = $(document.documentElement);
  function testaScroll(e) {
    if ( $window.scrollTop() == 0 ) {
      $html.removeClass('scrolled');
    } else {
      $html.addClass('scrolled');
    }
  }
  setTimeout(testaScroll, 300);
  window.addEventListener('scroll', testaScroll);
})();

$(()=>{
  $('.toggle-mobile-menu').on('click', function() {
    $('#theme-header nav').toggleClass('open');
  });
});
